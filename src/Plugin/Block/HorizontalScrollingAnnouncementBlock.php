<?php

namespace Drupal\horizontal_scrolling_announcement\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'HorizontalScrollingAnnouncement' Block.
 *
 * @Block(
 * id = "horizontal_scrolling_announcement",
 * admin_label = @Translation("Horizontal scrolling announcement"),
 * )
 */
class HorizontalScrollingAnnouncementBlock extends BlockBase implements BlockPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();

    if (!empty($config['hsa_scrollspeed'])) {
      $hsa_scrollspeed = $config['hsa_scrollspeed'];
    }
    else {
      $hsa_scrollspeed = "";
    }

    if (!empty($config['hsa_scrolldelay'])) {
      $hsa_scrolldelay = $config['hsa_scrolldelay'];
    }
    else {
      $hsa_scrolldelay = "";
    }

    if (!empty($config['hsa_direction'])) {
      $hsa_direction = $config['hsa_direction'];
    }
    else {
      $hsa_direction = "";
    }

    if (!empty($config['hsa_scrollgap'])) {
      $hsa_scrollgap = $config['hsa_scrollgap'];
    }
    else {
      $hsa_scrollgap = "";
    }

    if (!empty($config['hsa_announcement'])) {
      $hsa_announcement = $config['hsa_announcement'];
    }
    else {
      $hsa_announcement = "";
    }

    $output[]['#cache']['max-age'] = 0;
    $values = [
      'hsa_scrollspeed' => $hsa_scrollspeed,
      'hsa_scrolldelay' => $hsa_scrolldelay,
      'hsa_direction' => $hsa_direction,
      'hsa_scrollgap' => $hsa_scrollgap,
      'hsa_announcement' => $hsa_announcement,
    ];

    $markup = $this->hsaBlock($values);
    $output[] = [
      '#markup' => $markup,
      '#allowed_tags' => ['span', 'div', 'a', 'marquee', 'img', 'script', 'p'],
    ];
    $output['#attached']['library'][] = 'horizontal_scrolling_announcement/horizontal_scrolling_announcement';
    return $output;
  }

  /**
   * {@inheritdoc}
   */
  private function hsaBlock(array $values) {
    $speed = $values['hsa_scrollspeed'];
    $delay = $values['hsa_scrolldelay'];
    $direction = $values['hsa_direction'];
    $gap = $values['hsa_scrollgap'];
    $hsa_announcement = $values['hsa_announcement'];

    if (!is_numeric($speed)) {
      $speed = 20000;
    }

    if (!is_numeric($delay)) {
      $delay = 2000;
    }

    if ($direction != "left" && $direction != "right" && $direction != "up" && $direction != "down") {
      $direction = "left";
    }

    if (!is_numeric($gap)) {
      $gap = 50;
    }

    $target = "_self";
    $cnt = 0;
    $marquee = "";
    $hsa = "";

    $hsa_announcement_arry = explode("\n", $hsa_announcement);
    $hsa_announcement_arry = array_filter($hsa_announcement_arry, 'trim');
    foreach ($hsa_announcement_arry as $hsa_announcement) {
      $hsa_announcement = preg_replace("/\r|\n/", "", $hsa_announcement);

      if (strpos($hsa_announcement, ']-[') !== FALSE) {
        $hsa_announcement = explode("]-[", $hsa_announcement);
        $announcement_txt = $hsa_announcement[0];
        $announcement_lnk = $hsa_announcement[1];
      }
      elseif (strpos($hsa_announcement, '] - [') !== FALSE) {
        $hsa_announcement = explode("] - [", $hsa_announcement);
        $announcement_txt = $hsa_announcement[0];
        $announcement_lnk = $hsa_announcement[1];
      }
      elseif (strpos($hsa_announcement, ']- [') !== FALSE) {
        $hsa_announcement = explode("]- [", $hsa_announcement);
        $announcement_txt = $hsa_announcement[0];
        $announcement_lnk = $hsa_announcement[1];
      }
      elseif (strpos($hsa_announcement, '] -[') !== FALSE) {
        $hsa_announcement = explode("] -[", $hsa_announcement);
        $announcement_txt = $hsa_announcement[0];
        $announcement_lnk = $hsa_announcement[1];
      }
      else {
        if ($hsa_announcement <> "") {
          $announcement_txt = $hsa_announcement;
          $announcement_lnk = "";
        }
      }

      $announcement_txt = ltrim($announcement_txt, '[');
      $announcement_txt = rtrim($announcement_txt, ']');

      $announcement_lnk = ltrim($announcement_lnk, '[');
      $announcement_lnk = rtrim($announcement_lnk, ']');

      if ($announcement_lnk != "") {
        $hsa = $hsa . "<a target='" . $target . "' href='" . $announcement_lnk . "'>";
      }

      $hsa = $hsa . stripslashes($announcement_txt);

      if ($announcement_lnk != "") {
        $hsa = $hsa . "</a>";
      }

      $hsa = $hsa . '&nbsp;&nbsp;';

      $cnt = $cnt + 1;
    }

    $randnumber = mt_rand(10, 100);

    $height = "";
    if ($direction == "up" || $direction == "down") {
      $height = "height: 5px";
    }

    $marquee = $marquee . "<div class='marquee-" . $randnumber . "' style='width: 100%;overflow: hidden;" . $height . "'>";
    $marquee = $marquee . $hsa;
    $marquee = $marquee . "</div>";

    $marquee = $marquee . '<script type="text/javascript">';
    $marquee = $marquee . 'jQuery.noConflict(); ';
    $marquee = $marquee . 'jQuery(function() { ';
    $marquee = $marquee . "jQuery('.marquee-" . $randnumber . "').marquee({ ";
    $marquee = $marquee . 'allowCss3Support: true, ';
    $marquee = $marquee . "css3easing: 'linear', ";
    $marquee = $marquee . "easing: 'linear', ";
    $marquee = $marquee . "delayBeforeStart: " . $delay . ", ";
    $marquee = $marquee . "direction: '" . $direction . "', ";
    $marquee = $marquee . 'duplicated: true, ';
    $marquee = $marquee . "duration: " . $speed . ", ";
    $marquee = $marquee . "gap: " . $gap . ", ";
    $marquee = $marquee . 'pauseOnCycle: true, ';
    $marquee = $marquee . 'pauseOnHover: true, ';
    $marquee = $marquee . 'startVisible: true ';
    $marquee = $marquee . '}); ';
    $marquee = $marquee . '}); ';
    $marquee = $marquee . '</script>';

    return $marquee;

  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    $form['hsa_scrollspeed'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Scroll speed'),
      '#description' => $this->t('Please enter duration in milliseconds of the marquee in milliseconds, This will make the scroll faster. (Example: 20000)'),
      '#default_value' => isset($config['hsa_scrollspeed']) ? $config['hsa_scrollspeed'] : '20000',
    ];

    $form['hsa_scrolldelay'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Scroll delay'),
      '#description' => $this->t('Please enter pause time before the next animation turn in milliseconds. (Example: 2000)'),
      '#default_value' => isset($config['hsa_scrolldelay']) ? $config['hsa_scrolldelay'] : '2000',
    ];

    $options = [
      'left' => $this->t('scroll from right to left'),
      'right' => $this->t('scroll from left to right'),
      'up' => $this->t('scroll from down to up'),
      'down' => $this->t('scroll from up to down'),
    ];

    $form['hsa_direction'] = [
      '#type' => 'radios',
      '#title' => $this->t('Scroll direction'),
      '#options' => $options,
      '#description' => $this->t('Please select your direction for scrolling.'),
      '#default_value' => isset($config['hsa_direction']) ? $config['hsa_direction'] : 'left',
    ];

    $form['hsa_scrollgap'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Scroll gap'),
      '#description' => $this->t('Please enter gap in pixels between the announcement.'),
      '#default_value' => isset($config['hsa_scrollgap']) ? $config['hsa_scrollgap'] : '50',
    ];

    $form['hsa_announcement'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Announcementc text'),
      '#description' => $this->t('Please enter your scrolling announcement text. One announcement per line. <br /> [Your announcement text]-[When someone clicks on the announcement, where do you want to send them] <br /> Example : [Congratulations, you just completed configure]-[http://www.gopiplus.com/]'),
      '#default_value' => isset($config['hsa_announcement']) ? $config['hsa_announcement'] : '[Congratulations, you just completed configure.]-[http://www.gopiplus.com/extensions/]',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['hsa_scrollspeed'] = $form_state->getValue('hsa_scrollspeed');
    $this->configuration['hsa_scrolldelay'] = $form_state->getValue('hsa_scrolldelay');
    $this->configuration['hsa_direction'] = $form_state->getValue('hsa_direction');
    $this->configuration['hsa_scrollgap'] = $form_state->getValue('hsa_scrollgap');
    $this->configuration['hsa_announcement'] = $form_state->getValue('hsa_announcement');
  }

}
