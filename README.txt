CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Features
 * Requirements
 * Installation
 * Configuration
 * Maintainersss

INTRODUCTION
------------

Horizontal scrolling announcement drupal module lets scroll the content 
from one end to another end like reel, it is using JQuery Marquee 
script for scrolling. This is the simple way to create scrolling text 
in your website. In administrator section we have option to update 
scroll speed and direction.

FEATURES:
---------

1. Free module.
2. Easy to customize.
3. It supports all major browsers.
4. Configurable scroll speed.
5. Pause the scroll on mouse over.

REQUIREMENTS
------------

Just Drupal module Horizontal scrolling announcements moudule files

INSTALLATION
------------

1. Install the module as normal, see link for instructions.
Link: https://www.drupal.org/documentation/install/
modules-themes/modules-8

CONFIGURATION
-------------

1. Go to Admin >> Extend page and check the Enabled check box near 
to the module Horizontal scrolling announcement and then click the 
Install button at the bottom.

2. Go to Admin >> Structure >> Block layout page, there you can see 
button called Place Block near each available blocks.

3. Go to Admin >> Structure >> Blocks layout page and click Place 
Block button to add Block. If you want to add Block in your Breadcrumb, 
click Place Block button near your Breadcrumb title. It will open 
Place block window. In that window again click Place Block button near 
Horizontal scrolling announcement.

4. Now open your website front end and see Horizontal scrolling announcement 
module in the selected location. go to configuration link of the module 
and update the default text message.

MAINTAINERS
-----------

Current maintainers:

 * Gopi RAMASAMY 
https://www.drupal.org/user/1388160
http://www.gopiplus.com/work/about/
http://www.gopiplus.com/extensions/2017/
02/drupal-module-horizontal-scrolling-announcement/
 
Requires - Drupal 8
License - GPL (see LICENSE)
